﻿using System;

namespace _Amirul_VisualStudioGit
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //Exercise 1
            Console.WriteLine("1. Write a C# Sharp program to print Hello and your name in a separate line." +
                              "\n Hello Amirul!");

            //Exercise 2
            int firstNum = 8;
            int secondNum = 2;
            int sum = 0;

            sum = firstNum + secondNum;

            Console.WriteLine("2. Write a C# Sharp program to print the sum of two numbers." +
                              "\n The sum of 8 + 2 is " + sum);

            //Exercise 3

            sum = firstNum / secondNum;

            Console.WriteLine("3. Write a C# Sharp program to print the result of dividing two numbers." +
                              "\n The result 8 / 2 is " + sum);

            //Exercise 4
            Console.WriteLine("4. Write a C# Sharp program to print the result of the specified operations./n" +
                              "\n The result of -1 + 4 * 6 is " + (-1 + 4 * 6)+
                              "\n The result of (35 + 5) % 7 is " + ((35 + 5) % 7) +
                              "\n The result of 14 + -4 * 6 / 11 is " + (14 + -4 * 6 / 11) +
                              "\n The result of 2 + 15 / 6 * 1 - 7 % 2 is " + (2 + 15 / 6 * 1 - 7 % 2));
        }
    }
}
